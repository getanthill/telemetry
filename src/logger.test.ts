import { build } from './logger';

describe('logger', () => {
  describe('#sanitize', () => {
    it('returns log without modification on clear message', () => {
      const logger = build();

      const a = logger.format.transform({
        message: 'test',
        level: 'info',
      });

      expect(a).toMatchObject({
        message: 'test',
        level: 'info',
      });
    });

    it('returns log without cookie property', () => {
      const logger = build();

      const a = logger.format.transform({
        message: 'test',
        level: 'info',
        cookie: 'my-private-cookie',
      });

      expect(a).toMatchObject({
        message: 'test',
        level: 'info',
        cookie: '>> sensitive <<',
      });
    });

    it('returns log without `token` property', () => {
      const logger = build();

      const a = logger.format.transform({
        message: 'test',
        level: 'info',
        'User-Token': 'my-private-token',
      });

      expect(a).toMatchObject({
        message: 'test',
        level: 'info',
        'User-Token': '>> sensitive <<',
      });
    });
  });
});

import * as telemetry from '../src';

async function main() {
  console.log('Testing metrics');

  telemetry.start();
  
  const incrementHttpRequest = telemetry.metrics.createCounter(
    'http_requests_total',
    'Total number of HTTP requests'
  );

  // No attributes with implicit 1 increment
  incrementHttpRequest();
  // Explicit attributes with implicit 1 increment
  incrementHttpRequest({
    state: 'success'
  });
  // Explicit attributes & increment
  incrementHttpRequest({
    state: 'error'
  }, 5);

  const recordHttpRequestDuration = telemetry.metrics.createHistogram(
    'http_requests_duration_ms', // name
    'Duration of the HTTP requests in ms', // description
    [1, 100, 250, 500, 1000, 3000], // buckets
    'ms' // unit
  );

  // Explicit value without attributes
  recordHttpRequestDuration(123);
  // Explicit value and attributes
  recordHttpRequestDuration(534, {
    user_category: 'admin'
  });
  recordHttpRequestDuration(234, {
    user_category: 'patient'
  });
  recordHttpRequestDuration(643);
  recordHttpRequestDuration(1245);

  telemetry.metrics.createCounter('unused', 'This should not be shown');

  console.log(await fetch('http://localhost:9464/metrics').then((res) => res.text()));
}

main()
  .then(() => {
    console.log('Testing ended');

    process.exit(0);
  })
  .catch((err) => {
    console.error(err);

    process.exit(1);
  });

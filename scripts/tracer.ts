import telemetry from '../src';

async function main() {
  console.log('Testing tracer');

  console.log(telemetry);
}

main()
  .then(() => {
    console.log('Testing ended');

    process.exit(0);
  })
  .catch((err) => {
    console.error(err);

    process.exit(1);
  });

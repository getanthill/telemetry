import telemetry, { logger } from '../src';

import type { Express, Request, Response } from 'express';

import axios from 'axios';
import * as express from 'express';

const PORT: number = parseInt(process.env.PORT || '8080');
const app: Express = express();

const tracer = telemetry.api.trace.getTracer('my-service-tracer');

const client = axios.create({
  baseURL: 'http://localhost:8082',
});

app.get('/', (req: Request, res: Response) => {
  logger.info('Hello world');
  res.send('Hello World');
});

app.get('/pong', (req: Request, res: Response) => {
  res.send('pong');
});

app.get('/error', (req: Request, res: Response) => {
  const err = new Error('Ooops');
  const span = telemetry.api.trace.getActiveSpan();
  span?.recordException(err);
  span?.setStatus({ code: telemetry.api.SpanStatusCode.ERROR });
  res.status(500).send({ sent: true });
  span?.end();
});

app.get('/ping', (req: Request, res: Response) => {
  tracer.startActiveSpan('ping', (span) => {
    telemetry.api.trace.getActiveSpan()?.addEvent('/ping', {
      'http.method': req.method,
    });

    client
      .request({
        url: '/pong',
      })
      .then((_res) => {
        // Do stuff with response
        res.send(_res.data);
        span.end();
      });
  });
});

app.listen(PORT, () => {
  console.log(`Listening for requests on http://localhost:${PORT}`);
});
